#include <iostream>

using namespace std;

template <class T>
class node
{
public:
	T key;
	T/*unsigned char*/ height;
	node* left;
	node* right;

	

	node(T);
	T/*unsigned char*/ getheight(node*);
	T bfactor(node*);
	void fixheight(node*);
	node* rotateright(node*);
	node* rotateleft(node*);
	node* balance(node*);
	node* insert(node*, T);
	node* findmin(node*);
	node* removemin(node*);
	node* remove(node*, T);
	void inorder(node*);
};

template <class T>
node<T>::node(T k)
{
	key = k;
	left = right = 0;
	height = 1;
}

template <class T>
T node<T>::getheight(node* p)
{
	return p ? p->height : 0;
}

template <class T>
T node<T>::bfactor(node* p)
{
	return getheight(p->right) - getheight(p->left);
}

template <class T>
void node<T>::fixheight(node* p)
{
	T/*unsigned char*/ hl = getheight(p->left);
	T/*unsigned char*/ hr = getheight(p->right);
	p->height = (hl>hr ? hl : hr);
}

template <class T>
node<T>* node<T>::rotateright(node* p)
{
	node* q = p->left;
	p->left = q->right;
	q->right = p;
	fixheight(p);
	fixheight(q);
	return q;
}

template <class T>
node<T>* node<T>::rotateleft(node* p)
{
	node* q = p->right;
	p->right = q->left;
	q->left = p;
	fixheight(p);
	fixheight(q);
	return q;
}

template <class T>
node<T>* node<T>::balance(node* p)
{
	fixheight(p);
	if (bfactor(p) == 2)
	{
		if (bfactor(p->right) < 0)
		{
			p->right = rotateright(p->right);
		}
		return rotateleft(p);
	}
	if (bfactor(p) == -2)
	{
		if (bfactor(p->left) < 0)
		{
			p->left = rotateleft(p->left);
		}
		return rotateright(p);
	}
	return p;
}

template <class T>
node<T>* node<T>::insert(node* p, T k)
{
	if (!p)
	{
		return new node(k);
	}
	if (k<p->key)
	{
		p->left = insert(p->left, k);
	}
	else
	{
		p->right = insert(p->right, k);
	}
	return balance(p);
}

template <class T>
node<T>* node<T>::findmin(node* p)
{
	return p->left ? findmin(p->left) : p;
}

template <class T>
node<T>* node<T>::removemin(node* p)
{
	if (p->left == 0)
	{
		return p->right;
	}
	p->left = removemin(p->left);
	return balance(p);
}

template <class T>
node<T>* node<T>::remove(node* p, T k)
{
	if (!p)
	{
		return 0;
	}
	if (k < p->key)
	{
		p->left = remove(p->left, k);
	}
	else if (k > p->key)
	{
		p->right = remove(p->right, k);
	}
	else
	{
		node* q = p->left;
		node* r = p->right;
		delete p;
		if (!r)
		{
			return q;
		}
		node* min = findmin(r);
		min->right = removemin(r);
		min->left = q;
		return balance(min);
	}
	return balance(p);
}

template <class T>
void node<T>::inorder(node* p)
{

	if (p)
	{
		inorder(p->left);
		cout << p->key << " ";
		inorder(p->right);
	}
}

int main()
{
	node<int>* n = new node<int>(5);
	int array[10] = { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	for (int i = 0; i<10; i++)
		n = n->insert(n, array[i]);
	n->inorder(n);
	cout << "\n";
	n->remove(n, 12);
	n->inorder(n);
	getchar();
	node<char>* n1 = new node<char>('y');
	char array1[10] = { '6', '7', '8', '9', '0', '1', '2', 'h', '4', '5' };
	for (int i = 0; i<10; i++)
		n1 = n1->insert(n1, array1[i]);
	n1->inorder(n1);
	cout << "\n";
	n1->remove(n1, '12');
	n1->inorder(n1);
	getchar();
	system("pause");
	return 0;
}
